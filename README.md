**Desarrollo de aplicaciones web**

**Alex Ivan Sanchez Miranda**

**5AVP**

Practica #1 - 02/09/2022 - Practica de ejemplo
Commit: 59c30433c17eff3d8b3428c9dbedaef5515466c3

Practica #2 - 09/09/2022- practica JavaScript
commit:b0d240ca055cbdc80b7b249c782c702e77abccbe

Practica #3 - 15/09/2022 - Práctica web con base de datos - parte 1
commit: dbd9b3a540a2825b49a59ede14615dc5f982eeef

Practica #4 - 19/09/2022 - Práctica web con base de datos - vista de consulta de base de datos
commit: 7a438bedec88d5a8640dab8a9962e97c760d8325

Práctica #5 - 22/09/2022 - Práctica web con base de datos - Vista de registro de datos
commit: 2308e2c5021b0fb9cc5b619ac6a339b1fee3b19c

Práctica #6 - 26/09/2022 - Práctica web con base de datos - Conexion con base de datos
commit: 8d1ce745d0dcbfeda4795d1f4a28f6dbeb14bb31
